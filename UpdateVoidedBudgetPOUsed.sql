USE [OA]
GO
/****** Object:  StoredProcedure [ERP].[usp_UpdateVoidedBudgetPOUsed]    Script Date: 2019/4/23 下午 01:32:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Louis
-- Create date: 2018/11/01
-- Description:	作廢請購及採購信用卡部預算動支明細
-- =============================================
ALTER PROCEDURE [ERP].[usp_UpdateVoidedBudgetPOUsed] 
		 @SourceFormNum varchar(20)
		,@SourceDeliveryNo int
		,@TransferDate date
		,@FormStatus int
		,@FormCategory int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare	 @count int
	Declare @Result table (ReturnStatus varchar(50),ReturnMessage varchar(100),FormNum varchar(20),DeliveryNo int)
if @FormCategory = 1   --作廢請購
	begin
		insert into CreditDepBudgetDetail(
			CostProfitCenter 
			,CostProfitCenterName
			,FormCategory
			,ProjectCategoryID
			,FormStatus
			,TransferDate
			,BudgetAmount   
			,Included
			,FormKind
			,SourceFormNum
			,SourceDeliveryNo
			,FormRole
			,CreateTime 
		)
		Select top 1 CostProfitCenter 
			,CostProfitCenterName
			,FormCategory
			,ProjectCategoryID
			,FormStatus = @FormStatus
			,TransferDate = @TransferDate    
			,BudgetAmount = -(BudgetAmount)
			,Included
			,FormKind
			,SourceFormNum = @SourceFormNum
			,SourceDeliveryNo = @SourceDeliveryNo
			,2
			,Getdate()
		From CreditDepBudgetDetail where FormNum = @SourceFormNum and DeliveryNo = @SourceDeliveryNo and FormCategory = @FormCategory and FormStatus = 1    
	end
else
	begin
		if @FormCategory = 2  --作廢採購
			begin
				insert into CreditDepBudgetDetail(
					CostProfitCenter 
					,CostProfitCenterName
					,FormCategory
					,ProjectCategoryID
					,FormStatus
					,TransferDate
					,BudgetAmount   
					,Included
					,FormKind
					,SourceFormNum
					,SourceDeliveryNo
					,FormRole
					,CreateTime 
				)
				Select top 1 CostProfitCenter 
					,CostProfitCenterName
					,FormCategory
					,ProjectCategoryID
					,FormStatus = @FormStatus
					,TransferDate = @TransferDate    
					,-(BudgetAmount)
					,0
					,FormKind
					,SourceFormNum = @SourceFormNum
					,SourceDeliveryNo = @SourceDeliveryNo
					,3
					,Getdate()
				From CreditDepBudgetDetail where FormNum = @SourceFormNum and DeliveryNo = @SourceDeliveryNo and FormCategory = @FormCategory and FormStatus = 1    
			end
	end
   	if @@ROWCOUNT = 0
		begin
			insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E6','寫入資料庫失敗',@SourceFormNum,@SourceDeliveryNo)
		end

	--Select * from @Result
	select @count = Count(*) from @Result
	if @count > 0
		return 1
	else
		return 0

END
