USE [OA]
GO
/****** Object:  StoredProcedure [ERP].[usp_InsertBudgetAPEPOUsed]    Script Date: 2019/4/23 下午 01:36:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Louis
-- Create date: 2018/12/03
-- Description:	新增信用卡部預算動支明細檔-廠商請款-請購:傳送及銷案
-- =============================================
ALTER PROCEDURE [ERP].[usp_InsertBudgetAPEPOUsed]  
		 @FormNum varchar(20)                --請款單編號 
		,@DeliveryNo int                     --請款明細
		,@TransferDate date
		,@FormCategory int                   --1.PR,2.PO,3.AP
		,@FormStatus int                     --1.傳送,3.銷案
		,@FormKind int
		,@SourceFormNum varchar(20)          --採購單編號 
		,@SourceDeliveryNo int               --採購單送貨編號  
		,@Quantity Decimal(19,6)             --請款數量 
AS
Begin
Declare	 @count int
		,@ChargeDept  varchar(10)
		,@ChargeDeptName nvarchar(50)
		,@ProjectCategoryID uniqueidentifier
		,@BudgetAmount decimal(15,4)
		,@ReleaseAmount decimal(15,4)
		,@OverBudget int
		,@BudgetYearMonth varchar(6)
	    ,@UomCode varchar(10)
		,@ExchangeRate Decimal(15,7)
		,@UnitPrice Decimal(15,4)


Set @BudgetYearMonth = cast(Year(@TransferDate) as varchar) + RIGHT(REPLICATE('0', 2) + cast(Month(@TransferDate) as varchar),2)
--定義回傳結果變數Table
Declare @Result table (ReturnStatus varchar(50),ReturnMessage nvarchar(100),FormNum varchar(20),DeliveryNo int)

--檢查是否有此年度議價核發單或採購單送貨層資料
If Charindex('PO' ,@SourceFormNum)  > 0
	Begin
		SELECT @count = count(*) From PurchaseOrderDelivery Where IsDelete = 0 And PODeliveryID = @SourceDeliveryNo 
		print N'採購單送貨層:' + Ltrim(str(@count))
		--採購單釋回金額
		Select @UomCode = UomCode,@ExchangeRate = ExchangeRate From PurchaseRequisitionDetail Where PRDetailID In(
		Select PRDetailID From PurchaseRequisitionDelivery Where PRDeliveryID in(
		Select PRDeliveryID From PurchaseOrderDelivery Where IsDelete = 0 And PODeliveryID = @SourceDeliveryNo ))
		If @UomCode = 'AMT'
			Begin
				--原動支金額為釋回金額
				Set @ReleaseAmount =  (Select Top 1 BudgetAmount From CreditDepBudgetDetail Where FormNum = @SourceFormNum And DeliveryNo = @SourceDeliveryNo And FormCategory = 2 AND FormStatus = 1) 
			End
        Else
			Begin
				Select @UnitPrice = UnitPrice From PurchaseOrderDetail Where PODetailID In(
				SELECT PODetailID From PurchaseOrderDelivery Where IsDelete = 0 And PODeliveryID = @SourceDeliveryNo )
				Set @ReleaseAmount = @Quantity * @UnitPrice * @ExchangeRate   --請款數量 X 單價 X 數量
			End
		   
	
		print N'釋回金額' + ltrim(str(@ReleaseAmount))
	
		--請款單動支金額
		Set @BudgetAmount = (Select Top 1 TWDAmount From EPODetail Where IsDelete = 0 And LineNum = @DeliveryNo )


		print N'動支金額' + ltrim(str(@BudgetAmount))
        --定義動支或釋回正負
		if @FormStatus = 1     --傳送
			begin
				Set @ReleaseAmount = - @ReleaseAmount
			end
		else
			begin
				if @FormStatus = 3   --銷案
					begin
						set @BudgetAmount = -@BudgetAmount 
					end
			end
	End
else
	Begin
		if Charindex('BPR' ,@SourceFormNum)  > 0
			Begin
				SELECT @count = count(*) From YearlyApprovedDelivery Where IsDelete = 0 And YADeliveryID = @SourceDeliveryNo 
				print N'年度議價核發送貨層:' + Ltrim(str(@count))
				--年度議價核發釋回金額,依據請款的數量釋回
				Set @ReleaseAmount = (Select Top 1 UNIT_PRICE * (Select top 1 ExchangeRate From YearlyContract Where YCID in (Select YCID From YearlyApproved Where YAID = YA.YAID)) From YearlyApprovedDetail YA Where YADetailID in 
				(Select top 1 YADetailID From YearlyApprovedDelivery where YADeliveryID = @SourceDeliveryNo )) * @Quantity

				print N'釋回金額' + ltrim(str(@ReleaseAmount))
	
				--請款單動支金額
				Set @BudgetAmount = (Select Top 1 TWDAmount From EPODetail Where IsDelete = 0 And LineNum = @DeliveryNo )
				print N'動支金額' + ltrim(str(@BudgetAmount))

		        --定義動支或釋回正負
				if @FormStatus = 1   --傳送
					begin
						Set @ReleaseAmount = - @ReleaseAmount
					end
				else
					Begin
					if @FormStatus = 3   --銷案
						begin
							set @BudgetAmount = -@BudgetAmount 
						end
					End
			End
	End
if @count = 0
	begin
		insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E1',N'無送貨層資料',@FormNum,@DeliveryNo)
	end

set @count = 0 

--檢查是否有廠商請款-請款明細資料
SELECT @count = Count(*) From EPODetail where IsDelete = 0 And LineNum = @DeliveryNo
print N'請購明細:' + Ltrim(str(@count))
if @count = 0
	begin
		insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E2',N'無請購明細',@FormNum,@DeliveryNo)
	end

--檢查行銷活動專案
	if Charindex('PO' ,@SourceFormNum)  > 0  --查詢採購相關
		Begin
			Select top 1 @ProjectCategoryID = ProjectCategoryID from PurchaseRequisition Where PRID in 
		    (Select top 1 PRID from PurchaseRequisitionDetail Where IsDelete = 0 And PRDetailID in      
				(Select top 1 PRDetailID from PurchaseRequisitionDelivery where IsDelete = 0 And PRDeliveryID  in   
					(Select Top 1 PRDeliveryID  From PurchaseOrderDelivery  Where IsDelete = 0 And PODeliveryID = @SourceDeliveryNo)))

			print N'採購單行銷活動專案:' + Ltrim(str(@count)) + cast(@ProjectCategoryID as varchar(100))
		End
	else
	begin
		if Charindex('BPR' ,@SourceFormNum)  > 0  --查詢年度議價核發
			Begin
			Select top 1 @ProjectCategoryID = ProjectCategoryID from PurchaseRequisition Where PRID in 
		    (Select top 1 PRID from PurchaseRequisitionDetail Where IsDelete = 0 And PRDetailID in      
				(Select top 1 PRDetailID from PurchaseRequisitionDelivery where IsDelete = 0 And PRDeliveryID  in   
					(Select Top 1 PRDeliveryID  From YearlyApprovedDelivery  Where IsDelete = 0 And YADeliveryID = @SourceDeliveryNo)))
			print N'年度議價核發行銷活動專案:' + Ltrim(str(@count)) + cast(@ProjectCategoryID as varchar(100))
			End
	end

--檢查成本與利潤中心
if Charindex('PO' ,@SourceFormNum)  > 0  --查詢採購相關
	begin
		--************應該從原動支記錄抓*******************************	
		Select top 1 @ChargeDept = CostProfitCenter,@ChargeDeptName = CostProfitCenterName From CreditDepBudgetDetail Where FormNum = @SourceFormNum And DeliveryNo = @SourceDeliveryNo And FormCategory = 2 AND FormStatus = 1
		print N'採購單成本與利潤中心:' + Ltrim(str(@count)) + @ChargeDept + '-' + @ChargeDeptName
	end
else
	begin
	if Charindex('BPR' ,@SourceFormNum)  > 0 --查詢年度議價核發相關
		begin
		--************   應該從原動支記錄抓
		--@SourceFormNum是年度議價協議+核發次數可否抓到原動支金額，因為傳進來可能是年度議價核發編號
		--*******************************	
			Select top 1 @ChargeDept = CostProfitCenter,@ChargeDeptName = CostProfitCenterName From CreditDepBudgetDetail Where FormNum = @SourceFormNum And DeliveryNo = @SourceDeliveryNo And FormCategory = 2 AND FormStatus = 1
			print N'年度議價核發成本與利潤中心:' + Ltrim(str(@count)) + @ChargeDept + '-' + @ChargeDeptName
			--來源表單編號是年度議價協議+核發次數
			declare @BpaNumReleaseNum varchar(20)
			Select @BpaNumReleaseNum = YC.BpaNum + Ltrim(CAST(YA.ReleaseNum as varchar(5))) from YearlyContract YC inner join YearlyApproved YA on yc.YCID = YA.YCID  where YA.BprNum =  @SourceFormNum
			Set @SourceFormNum = @BpaNumReleaseNum
		end
	end

	--從分攤明細取得成本利潤中心代碼

  Set @count = 0
  Select @count = Count(*) from @Result
  if @count = 0
	Begin
	--廠商請款-請購:傳送或銷案
	Declare @apErr int,@poErr int
	--定義ID變數
	declare @ADetailID int,@OriginalAmortizationTWDAmount bigint,@ExpenseAttribute varchar(2),@ExpenseAttributeName nvarchar(100)
	--檢查是否超過可用預算
	DECLARE	@Remaining decimal(15,4)
		--廠商請款-請購:傳送或銷案
		--Declare @apErr int,@poErr int
		Begin Transaction
		-- 採購單預算釋回
		INSERT INTO ERP.CreditDepBudgetDetail(
				CostProfitCenter 
			,CostProfitCenterName 
			,ProjectCategoryID	
			,TransferDate	
			,FormCategory	
			,FormNum	
			,DeliveryNo	
			,FormStatus	
			,BudgetAmount	
			,Included
			,FormKind
			,SourceFormNum
			,SourceDeliveryNo
			,FormRole
			,CreateTime	
			)
		VALUES (
				@ChargeDept 
				,@ChargeDeptName 
				,@ProjectCategoryID
				,@TransferDate	
				,2	             --PO.採購單
				,@FormNum	
				,@DeliveryNo	
				,@FormStatus	
				,@ReleaseAmount
				,0              --列入預算控管      
				,@FormKind
				,@SourceFormNum
				,@SourceDeliveryNo
				,4             --請款經辦   
			,GETDATE()	
			)
		set @poErr = @@ERROR

		--廠商請款請購-動支金額

			--定義Cursor並打開
			DECLARE Amo Cursor FOR --宣告，名稱為MyCursor
			select ExpenseAttribute,ExpenseAttributeName, ADetailID,OriginalAmortizationTWDAmount,CostProfitCenter,CostProfitCenterName from AmortizationDetail where LineNum = @DeliveryNo And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 
			Open Amo 
			print @@CURSOR_rows --查看總筆數
			--定義ID變數
			--declare @ADetailID int,@OriginalAmortizationTWDAmount bigint 

			--開始迴圈跑Cursor Start
			Fetch NEXT FROM Amo INTO @ExpenseAttribute,@ExpenseAttributeName,@ADetailID,@OriginalAmortizationTWDAmount,@ChargeDept,@ChargeDeptName
			While (@@FETCH_STATUS <> -1)
			BEGIN
				set @BudgetAmount = @OriginalAmortizationTWDAmount
				if @FormStatus = 3
					begin
						set @BudgetAmount = -@BudgetAmount --如果是銷案變為負的預算釋回
					end
							INSERT INTO ERP.CreditDepBudgetDetail(
							CostProfitCenter 
							,CostProfitCenterName 
							,ExpenseAttribute
							,ExpenseAttributeName
							,ProjectCategoryID	
							,TransferDate	
							,FormCategory	
							,FormNum	
							,DeliveryNo	
							,FormStatus	
							,BudgetAmount	
							,Included
							,FormKind
							,SourceFormNum
							,SourceDeliveryNo
							,FormRole
							,CreateTime	
							)
						VALUES (
								@ChargeDept 
								,@ChargeDeptName 
								,@ExpenseAttribute
								,@ExpenseAttributeName
								,@ProjectCategoryID
								,@TransferDate	
								,@FormCategory	
								,@FormNum	
							,@ADetailID	
							,@FormStatus
							,@BudgetAmount
							,1               --列入預算控管
							,@FormKind
							,@SourceFormNum
							,@SourceDeliveryNo
							,4              --請款經辦
							,GETDATE()	
							)

							--判斷為本月首次動支記錄呼叫更新信用卡部門預算動支主檔
							Set @count = 0
							SELECT @count = Count(*) From CreditDepBudgetDetail Where CostProfitCenter = @ChargeDept And CostProfitCenter In (Select CostProfitCenter From CreditDepBudget Where BudgetYearMonth = @BudgetYearMonth)
							if @count = 0
								Begin
								 --更新信用卡部門預算動支主檔
								EXEC	@count = [ERP].[usp_UpdateCreditDepBudget]
										@TransferDate = @TransferDate,
										@CostProfitCenter = @ChargeDept
								End
							--更新月累記動支金額
							Update CreditDepBudget Set MonthAddBudget = (Select Sum(BudgetAmount) From CreditDepBudgetDetail Where CostProfitCenter = @ChargeDept And cast(Year(@TransferDate) as varchar) + RIGHT(REPLICATE('0', 2) + cast(Month(@TransferDate) as varchar),2) = @BudgetYearMonth And Included = 1)
									Where CostProfitCenter = @ChargeDept And BudgetYearMonth = @BudgetYearMonth
			Fetch NEXT FROM Amo INTO @ExpenseAttribute,@ExpenseAttributeName,@ADetailID,@OriginalAmortizationTWDAmount,@ChargeDept,@ChargeDeptName
			END

			--開始迴圈跑Cursor End

			--關閉&釋放cursor
			CLOSE Amo
			DEALLOCATE Amo

		set @apErr = @@ERROR
		if @apErr = 0 and @poErr = 0
			Begin
					set @count = @@RowCount
					Commit Transaction
			End
		else
			Begin
					set @count = 0
					insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E6',N'寫入資料庫失敗',@FormNum,@DeliveryNo)
					RollBack Transaction
			End
    end

  --若無錯誤更新當月預算主檔
  set @count = 0
  Select @count = Count(*) From @Result
  If @count = 0
	 Begin
	--從分攤明細取得成本利潤中心代碼
	   Select Top 1 @ChargeDept = CostProfitCenter from AmortizationDetail Where LineNum = @DeliveryNo
	   Print N'更新當月動支結果:' + @ChargeDept
		Update CreditDepBudget Set MonthAddBudget = (Select Sum(BudgetAmount) From CreditDepBudgetDetail Where CostProfitCenter = @ChargeDept And cast(Year(@TransferDate) as varchar) + RIGHT(REPLICATE('0', 2) + cast(Month(@TransferDate) as varchar),2) = @BudgetYearMonth And Included = 1)
			Where CostProfitCenter = @ChargeDept And BudgetYearMonth = @BudgetYearMonth
	 End

  --取得最後結果，若是無錯誤則回傳空值		
  --select * from @Result
	if @count > 0
		return 1
	else
		return 0
End
