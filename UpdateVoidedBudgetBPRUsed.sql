USE [OA]
GO
/****** Object:  StoredProcedure [ERP].[usp_UpdateVoidedBudgetPOUsed]    Script Date: 2019/4/23 下午 01:32:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Louis
-- Create date: 2018/11/01
-- Description:	作廢年度議價核發信用卡部預算動支明細
-- =============================================
ALTER PROCEDURE [ERP].[usp_UpdateVoidedBudgetPOUsed] 
		 @SourceFormNum varchar(20)
		,@SourceDeliveryNo int
		,@TransferDate date
		,@FormStatus int
		,@FormCategory int
		,@BpaFormNum varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare	 @count int
	Declare @Result table (ReturnStatus varchar(50),ReturnMessage varchar(100),FormNum varchar(20),DeliveryNo int)
    --取得原年度議價核發動支相關資料後，再將該資訊寫入釋回
    insert into CreditDepBudgetDetail(
        CostProfitCenter 
        ,CostProfitCenterName
        ,FormCategory
        ,ProjectCategoryID
        ,FormStatus
        ,TransferDate
        ,BudgetAmount   
        ,Included
        ,FormKind
        ,SourceFormNum
        ,SourceDeliveryNo
        ,FormRole
        ,CreateTime 
    )
    Select top 1 CostProfitCenter 
        ,CostProfitCenterName
        ,FormCategory
        ,ProjectCategoryID
        ,FormStatus = @FormStatus
        ,TransferDate = @TransferDate    
        ,-(BudgetAmount)
        ,0
        ,FormKind
        ,SourceFormNum = @BpaFormNum
        ,SourceDeliveryNo = @SourceDeliveryNo
        ,3
        ,Getdate()
    From CreditDepBudgetDetail where BprFormNum = @SourceFormNum and DeliveryNo = @SourceDeliveryNo and FormCategory = @FormCategory and FormStatus = 1    
   	if @@ROWCOUNT = 0
		begin
			insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E6','寫入資料庫失敗',@SourceFormNum,@SourceDeliveryNo)
		end

	--Select * from @Result
	select @count = Count(*) from @Result
	if @count > 0
		return 1
	else
		return 0

END
