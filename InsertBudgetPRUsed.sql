USE [OA]
GO
/****** Object:  StoredProcedure [ERP].[usp_InsertBudgetPRUsed]    Script Date: 2019/4/23 下午 01:29:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Louis
-- Create date: 2018/10/29
-- Description:	新增信用卡部預算動支明細檔-請購表單
-- =============================================
ALTER PROCEDURE [ERP].[usp_InsertBudgetPRUsed]  
		 @FormNum varchar(20)
		,@DeliveryNo int
		,@TransferDate date
		,@FormCategory int
		,@FormStatus int
		,@FormKind int
		,@FormRole int
AS
BEGIN
Declare	 @count int
		,@ChargeDept  varchar(10)
		,@ChargeDeptName nvarchar(50)
		,@ProjectCategoryID uniqueidentifier
		,@BudgetAmount decimal(15,4)
		,@ReleaseAmount decimal(15,4)

--回傳寫入請購動支結果		
Declare @Result table (ReturnStatus varchar(50),ReturnMessage nvarchar(100),FormNum varchar(20),DeliveryNo int)

		INSERT INTO ERP.CreditDepBudgetDetail(
			CostProfitCenter 
		,CostProfitCenterName 
		,ProjectCategoryID	
		,TransferDate	
		,FormCategory	
		,FormNum	
		,DeliveryNo	
		,FormStatus	
		,BudgetAmount	
		,Included
		,FormKind
		,CreateTime
		,FormRole	
		)
		VALUES (
			(Select top 1 ChargeDept  From PurchaseRequisitionDelivery Where PRDeliveryID = @DeliveryNo) 
		,(Select top 1 ChargeDeptName  From PurchaseRequisitionDelivery Where PRDeliveryID = @DeliveryNo) 
		,(Select top 1 ProjectCategoryID  From PurchaseRequisition Where  PRNum = @FormNum) 
		,@TransferDate	
		,@FormCategory	
		,@FormNum	
		,@DeliveryNo	
		,@FormStatus	
		,(Select Top 1 Price From PurchaseRequisitionDetail Where PRDetailID in (Select top 1 PRDetailID From PurchaseRequisitionDelivery where PRDeliveryID = @DeliveryNo )) 
		 * (Select top 1 Quantity From PurchaseRequisitionDelivery where PRDeliveryID = @DeliveryNo )			 	
		,0  --不列入預算控管     
		,@FormKind
		,GETDATE()
		,@FormRole	
		)
	if @@ROWCOUNT = 0
		begin
			insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E5',N'寫入資料庫失敗',@FormNum,@DeliveryNo)
		end

	--取得最後結果，若是無錯誤則回傳空值		
    select @count = Count(*) from @Result
	if @count > 0
		return 1   --寫入失敗
	else
		return 0   --寫入成功
End
