USE [OA]
GO
/****** Object:  StoredProcedure [ERP].[usp_GetCreditBudgetVerify]    Script Date: 2019/4/23 下午 01:40:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Louis
-- Create date: 2019/3/6
-- Description:	檢查是否超過部門預算金額
-- =============================================
ALTER PROCEDURE [ERP].[usp_GetCreditBudgetVerify]  
	-- Add the parameters for the stored procedure here
	@TransferDate date, @BudgetAmount decimal(15,4), @CostProfitCenter varchar(10)
AS
BEGIN
Declare  @BudgetYearMonth varchar(6), @CurrentMonthBudget decimal(15,4), @CurrentYearBudget decimal(15,4), @Result Bit
--Declare @trace table (Name varchar(100), Budget decimal(15,4))
Set @BudgetYearMonth = cast(Year(@TransferDate) as varchar) + RIGHT(REPLICATE('0', 2) + cast(Month(@TransferDate) as varchar),2) --取得預算年月
--【當月預算金額-當月新增預算】
Select @CurrentMonthBudget = MonthBudget - isnull(MonthAddBudget,0) from CreditDepBudget Where CostProfitCenter = @CostProfitCenter And BudgetYearMonth = @BudgetYearMonth
--insert @trace(Name,Budget) values('MonthBudget',@CurrentMonthBudget)
Print N'當月剩餘預算金額' + Convert(varchar(20),@CurrentMonthBudget)

--【年度累計預算金額-(期初已用預算+當月新增預算)】
Select @CurrentYearBudget = Isnull(C.YearlySumAmount,0)  - (ISNULL(C.BeginningBudget,0) - Isnull(C.MonthAddBudget,0)) from CreditDepBudget C Where CostProfitCenter = @CostProfitCenter And BudgetYearMonth = @BudgetYearMonth
--insert @trace(Name,Budget) values('YearBudget',@CurrentYearBudget)
Print N'年度剩餘預算金額' + Convert(varchar(20),@CurrentYearBudget)

if  @BudgetAmount > @CurrentMonthBudget
	begin
		if @BudgetAmount > @CurrentYearBudget
			begin
			  set @Result = @CurrentYearBudget
			end
		else
		   begin
			  set @Result = 0
		   end
	end
else
	begin
			  set @Result = 0
	end
	
	--Select * From @trace	
	Print N'剩餘可用預算' + Convert(varchar(20),@Result)
	Return @result
END
