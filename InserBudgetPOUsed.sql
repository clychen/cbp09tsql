USE [OA]
GO
/****** Object:  StoredProcedure [ERP].[usp_InsertBudgetPOUsed]    Script Date: 2019/4/23 下午 01:33:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Louis
-- Create date: 2018/11/07
-- Description:	新增信用卡部預算動支明細檔-採購或年度議價核發表單
-- =============================================
ALTER PROCEDURE [ERP].[usp_InsertBudgetPOUsed]  
		 @FormNum varchar(20)
		,@DeliveryNo int
		,@TransferDate date
		,@FormCategory int
		,@FormStatus int
		,@FormKind int
		,@SourceFormNum varchar(20)
		,@SourceDeliveryNo int
		,@BprFormNum varchar(20) null  --原始年度議價核發編號

AS
BEGIN
	Declare @Result table (ReturnStatus varchar(50),ReturnMessage nvarchar(100),FormNum varchar(20),DeliveryNo int)
	Declare @prErr int,@poErr int,@count int,@ChargeDept varchar(10),@ChargeDeptName nvarchar(50),@ProjectCategoryID uniqueidentifier,
			@BudgetAmount Decimal(15,4),@ReleaseAmount Decimal(15,4)
	Set @ChargeDept = (Select top 1 ChargeDept  From PurchaseRequisitionDelivery Where PRDeliveryID = @SourceDeliveryNo)
	Set @ChargeDeptName  = (Select top 1 ChargeDeptName  From PurchaseRequisitionDelivery Where PRDeliveryID = @SourceDeliveryNo)
	Set @ProjectCategoryID = (Select top 1 ProjectCategoryID from PurchaseRequisition Where PRID in 
						(Select top 1 PRID from PurchaseRequisitionDetail Where PRDetailID in      
							(Select top 1 PRDetailID from PurchaseRequisitionDelivery where PRDeliveryID = @SourceDeliveryNo)))

	Set @ReleaseAmount = -(Select Top 1 Price From PurchaseRequisitionDetail Where PRDetailID in 
		(Select top 1 PRDetailID From PurchaseRequisitionDelivery where PRDeliveryID = @SourceDeliveryNo )) *
		(Select top 1 Quantity From PurchaseRequisitionDelivery where PRDeliveryID = @SourceDeliveryNo )			 	


	if @FormKind = 2   --採購單
		begin
				Set @BudgetAmount = (Select Top 1 UnitPrice  From PurchaseOrderDetail Where PODetailID in 
						(Select top 1 PODetailID From PurchaseOrderDelivery where PODeliveryID = @DeliveryNo )) *
						(Select Top 1 ExchangeRate From PurchaseRequisitionDetail Where PRDetailID in (Select top 1 PRDetailID From PurchaseRequisitionDelivery where PRDeliveryID = @SourceDeliveryNo )) *
						(Select top 1 Quantity From PurchaseOrderDelivery where PODeliveryID = @DeliveryNo )
				Begin Transaction
				-- 新增採購單--動支金額
				INSERT INTO ERP.CreditDepBudgetDetail(
					 CostProfitCenter 
					,CostProfitCenterName 
					,ProjectCategoryID	
					,TransferDate	
					,FormCategory	
					,FormNum	
					,DeliveryNo	
					,FormStatus	
					,BudgetAmount	
					,Included
					,FormKind
					,SourceFormNum
					,SourceDeliveryNo
					,FormRole
					,CreateTime	
					)
				VALUES (
					 @ChargeDept
					,@ChargeDeptName 
					,@ProjectCategoryID 
					,@TransferDate	
					,@FormCategory	
					,@FormNum	
					,@DeliveryNo	
					,@FormStatus	
					,@BudgetAmount			 	
					,0
					,@FormKind
					,@SourceFormNum
					,@SourceDeliveryNo
					,2              --採購經辦
					,GETDATE()	
					)
				set @poErr = @@ERROR

				-- 新增採購單--沖銷請購動支金額
				INSERT INTO ERP.CreditDepBudgetDetail(
					 CostProfitCenter 
					,CostProfitCenterName 
					,ProjectCategoryID	
					,TransferDate	
					,FormCategory	
					,FormNum	
					,DeliveryNo	
					,FormStatus	
					,BudgetAmount	
					,Included
					,FormKind
					,SourceFormNum
					,SourceDeliveryNo
					,FormRole
					,CreateTime	
					)
				VALUES (
					 @ChargeDept
					,@ChargeDeptName 
					,@ProjectCategoryID 
					,@TransferDate	
					,1	
					,@FormNum	
					,@DeliveryNo	
					,@FormStatus
					,@ReleaseAmount
					,0
					,@FormKind
					,@SourceFormNum
					,@SourceDeliveryNo
					,2              --採購經辦
					,GETDATE()	
					)
				set @prErr = @@ERROR
				if @prErr = 0 and @poErr = 0
					Begin
					 set @count = 0
					 Commit Transaction
					End
				else
					Begin
					 set @count = 0
					 insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E6',N'寫入資料庫失敗',@FormNum,@DeliveryNo)
					 RollBack Transaction
					End
		end
	else  --年度議價核發預算動支記錄
		begin
				Set @BudgetAmount = (Select Top 1 UNIT_PRICE * (Select top 1 ExchangeRate From YearlyContract Where YCID in (Select YCID From YearlyApproved Where YAID = YA.YAID)) From YearlyApprovedDetail YA Where YADetailID in 
						(Select top 1 YADetailID From YearlyApprovedDelivery where YADeliveryID = @DeliveryNo )) *
						(Select top 1 ShipmentQuantity From YearlyApprovedDelivery where YADeliveryID = @DeliveryNo )			 	

				Begin Transaction
				-- 新增年度議價核發單--動支金額
				INSERT INTO ERP.CreditDepBudgetDetail(
					 CostProfitCenter 
					,CostProfitCenterName 
					,ProjectCategoryID	
					,TransferDate	
					,FormCategory	
					,FormNum	
					,DeliveryNo	
					,FormStatus	
					,BudgetAmount	
					,Included
					,FormKind
					,SourceFormNum
					,SourceDeliveryNo
					,FormRole
					,CreateTime
					,BprFormNum	
					)
				VALUES (
					@ChargeDept
					,@ChargeDeptName 
					,@ProjectCategoryID 
                    ,@TransferDate	
					,@FormCategory	
					,@FormNum	
					,@DeliveryNo	
					,@FormStatus	
					,@BudgetAmount					
					,0
					,@FormKind
					,@SourceFormNum
					,@SourceDeliveryNo
					,2              --採購經辦
					,GETDATE()
					,@BprFormNum    --增加年度議價核發原始表單編號	
					)
				set @poErr = @@ERROR

				-- 新增年度議價核發單--沖銷請購動支金額
				INSERT INTO ERP.CreditDepBudgetDetail(
					 CostProfitCenter 
					,CostProfitCenterName 
					,ProjectCategoryID	
					,TransferDate	
					,FormCategory	
					,FormNum	
					,DeliveryNo	
					,FormStatus	
					,BudgetAmount	
					,Included
					,FormKind
					,SourceFormNum
					,SourceDeliveryNo
					,FormRole
					,CreateTime	
					)
				VALUES (
					 @ChargeDept
					,@ChargeDeptName 
					,@ProjectCategoryID 
					,@TransferDate	
					,1	
					,@FormNum	
					,@DeliveryNo	
					,@FormStatus
					,@ReleaseAmount
					,0
					,@FormKind
					,@SourceFormNum
					,@SourceDeliveryNo
					,2              --採購經辦
					,GETDATE()	
					)
				set @prErr = @@ERROR
				if @prErr = 0 and @poErr = 0
					Begin
					 set @count = 0
					 Commit Transaction
					End
				else
					Begin
					 set @count = 0
					 insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E6',N'寫入資料庫失敗',@FormNum,@DeliveryNo)
					 RollBack Transaction
					End
        end

	--取得最後結果，若是無錯誤則回傳空值		
    select @count = Count(*) from @Result
	if @count > 0
		Begin
			Select * From @Result	
			return 1
		End
	else
		return 0

End