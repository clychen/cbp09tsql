USE [OA]
GO
/****** Object:  StoredProcedure [ERP].[usp_UpdateDismissalBudgetPRUsed]    Script Date: 2019/4/23 下午 01:30:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Louis
-- Create date: 2018/11/01
-- Description:	銷案信用卡部預算動支明細
-- =============================================
ALTER PROCEDURE [ERP].[usp_UpdateDismissalBudgetPRUsed] 
		 @FormNum varchar(20)
		,@TransferDate date
		,@FormStatus int
		,@DeliveryNo int
		,@FormCategory int
		,@FormRole int
AS
BEGIN
	Declare	 @count int
	Declare @Result table (ReturnStatus varchar(50),ReturnMessage varchar(100),FormNum varchar(20),DeliveryNo int)
	insert into CreditDepBudgetDetail(
			CostProfitCenter 
			,CostProfitCenterName
			,FormCategory
			,ProjectCategoryID
			,FormNum
			,DeliveryNo
			,FormStatus
			,TransferDate
			,BudgetAmount   
			,Included
			,FormKind
			,FormRole
			,CreateTime 
		)
		Select top 1 CostProfitCenter 
			,CostProfitCenterName
			,FormCategory = @FormCategory
			,ProjectCategoryID
			,FormNum
			,DeliveryNo
			,FormStatus = @FormStatus
			,TransferDate = @TransferDate    
			,BudgetAmount = -(BudgetAmount)
			,Included
			,FormKind
			,@FormRole
			,CreateTime = Getdate()
		From CreditDepBudgetDetail where FormNum = @FormNum and FormStatus = 1 and DeliveryNo = @DeliveryNo and FormCategory = @FormCategory   
		if @@ROWCOUNT = 0
			begin
				insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E6','寫入資料庫失敗',@FormNum,@DeliveryNo)
			end

	--取得最後結果，若是無錯誤則回傳空值		
	--select * from @Result
	select @count = Count(*) from @Result
	if @count > 0
		return 1
	else
		return 0

END