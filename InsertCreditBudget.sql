USE [OA]
GO
/****** Object:  StoredProcedure [ERP].[usp_InsertCreditBudget]    Script Date: 2019/4/23 下午 01:28:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Louis
-- Create date: 2019/03/08
-- Description:	寫入信用卡動支記錄
-- =============================================
ALTER PROCEDURE [ERP].[usp_InsertCreditBudget]  
	-- Add the parameters for the stored procedure here
	@FormID UniqueIdentifier
   ,@Action int                  --1.傳送,2.作廢,3.銷案
   ,@VoidedDeliveryNo int Null   --作廢送貨層系統編號
AS
BEGIN
	Declare @FormNum varchar(20)
			,@ExpenseKind varchar(15)
			,@DeliveryNo int
			,@SourceFormNum varchar(20)
			,@SourceDeliveryNo int
			,@FormKind int
			,@TransferDate date
			,@Count int
			,@return_value int 
		    ,@BpaNumReleaseNum varchar(20)

   Declare @Result Table (ReturnStatus int, RemainBudget Decimal(15,4))  --1.寫入成功,2.寫入失敗,3.超過可用預算

	--取得表單編號
		SELECT 
			@FormNum = iif(isnull(E2.PRNum,'') != '',E2.PRNum,
				iif(isnull(E3.PONum,'') != '',E3.PONum,
					iif(isnull(E4.BPRNum,'') != '',E4.BPRNum,
						iif(isnull(E5.EPONum,'') != '',E5.EPONum,
							iif(isnull(E6.ENPNum,'') != '',E6.ENPNum,null)))))
			FROM ERP.FormMaster E1 
			Left JOIN ERP.PurchaseRequisition E2 ON E1.FormID = E2.PRID
			Left JOIN ERP.PurchaseOrder E3 ON E1.FormID = E3.POID
			Left JOIN ERP.YearlyApproved E4 ON E1.FormID = E4.YAID
			Left JOIN ERP.EPOMaster E5 ON E1.FormID = E5.EPOID
			Left JOIN ERP.ENPMaster E6 ON E1.FormID = E6.EID
			Where E1.FormID = @FormID

    insert into @Result(ReturnStatus,RemainBudget) values(1,0)    --可用預算的紀錄


    -- 依據表單前置碼決定執行的SP 

--***  請採購預算動支 ***-- 
	--請購單:傳送
	----[ERP].[usp_InsertBudgetPRUsed]
	if (SUBSTRING(@FormNum,1,2) = 'PR' or SUBSTRING(@FormNum,1,3) = 'PRD') And @Action = 1
		begin
			--先將可能存在的動支記錄刪除
			Delete From CreditDepBudgetDetail Where FormNum = @FormNum And FormCategory = 1 And FormStatus = @Action

			--取得請購單的送貨層明細
			DECLARE PR_cursor CURSOR FOR   
			SELECT PRDeliveryID  
			FROM PurchaseRequisitionDelivery Where IsDelete = 0 And isnull(CancelReason,'') = '' And PRDetailID In
												(Select PRDetailID From PurchaseRequisitionDetail Where IsDelete = 0 And PRID in 
													(Select PRID From PurchaseRequisition where PRNum = @FormNum And ProjectCategoryID In 
														(Select ProjectCategoryID  From ProjectCategory Where Source = 'EPMS')))   --行銷活動專案類別
  
			OPEN PR_cursor  
  			FETCH NEXT FROM PR_cursor INTO @DeliveryNo  
  
			WHILE @@FETCH_STATUS = 0  
			BEGIN  
				Set @TransferDate = GETDate()
				EXEC	@return_value = [ERP].[usp_InsertBudgetPRUsed]
						@FormNum = @FormNum,
						@DeliveryNo = @DeliveryNo,
						@TransferDate = @TransferDate,
						@FormCategory = 1, --PR
						@FormStatus = @Action,   --傳送
						@FormKind = 1,     --請購單
						@FormRole = 1      --請購經辦
				--if @return_value > 0
				--	Print N'請購單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '動支記錄寫入結果:失敗' 
				--else
				--	Print N'請購單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '動支記錄寫入結果:成功' 
	  			FETCH NEXT FROM PR_cursor INTO @DeliveryNo  
            End
			CLOSE PR_cursor  
			DEALLOCATE PR_cursor  
		end
	--請購單:傳送(採購經辦更新) -->可以不需要
	----[ERP].[usp_UpdateBudgetPRUsed]
	
	--請購單:銷案
	----[ERP].[usp_UpdateDismissalBudgetPRUsed]
	if (SUBSTRING(@FormNum,1,2) = 'PR' or SUBSTRING(@FormNum,1,3) = 'PRD') And @Action = 3
		begin
			print N'請購單:銷案'
			--先將可能存在的動支記錄刪除
			Delete From CreditDepBudgetDetail Where FormNum = @FormNum And FormCategory = 1 And FormStatus = @Action
			--取得請購單的送貨層明細
			DECLARE PR_cursor CURSOR FOR   
			SELECT PRDeliveryID  
			FROM PurchaseRequisitionDelivery Where IsDelete = 0 And isnull(CancelReason,'') = '' And PRDetailID In
												(Select PRDetailID From PurchaseRequisitionDetail Where IsDelete = 0 And PRID in 
													(Select PRID From PurchaseRequisition where PRNum = @FormNum And ProjectCategoryID In 
														(Select ProjectCategoryID  From ProjectCategory Where Source = 'EPMS')))  --行銷活動專案類別
			OPEN PR_cursor  
  			FETCH NEXT FROM PR_cursor INTO @DeliveryNo  
  
			WHILE @@FETCH_STATUS = 0  
			BEGIN  
				Set @TransferDate = GETDate()
				EXEC @return_value = [ERP].[usp_UpdateDismissalBudgetPRUsed]
					@FormNum = @FormNum,
					@TransferDate = @TransferDate,
					@FormStatus = @Action,       --銷案
					@DeliveryNo = @DeliveryNo,
					@FormCategory = 1,     --PR
					@FormRole = 1          --請購經辦    
				--if @return_value > 0
				--	Print N'請購單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '動支記錄銷案結果:失敗' 
				--else
				--	Print N'請購單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '動支記錄銷案結果:成功' 
	  			FETCH NEXT FROM PR_cursor INTO @DeliveryNo  
            End
			CLOSE PR_cursor  
			DEALLOCATE PR_cursor  
		end

	--作廢請購明細
	----[ERP].[usp_UpdateVoidedBudgetPOUsed]
	if (SUBSTRING(@FormNum,1,2) = 'PR' or SUBSTRING(@FormNum,1,3) = 'PRD') And @Action = 2
		begin
			print N'作廢請購明細'
			--先將可能存在的動支記錄刪除
			Delete From CreditDepBudgetDetail Where SourceFormNum = @FormNum And DeliveryNo = @VoidedDeliveryNo And FormCategory = 1 And FormStatus = @Action
				Set @TransferDate = GETDate()
				EXEC	@return_value = [ERP].[usp_UpdateVoidedBudgetPOUsed]
						@SourceFormNum = @FormNum,
						@SourceDeliveryNo = @VoidedDeliveryNo,
						@TransferDate = @TransferDate,
						@FormStatus = @Action,          --作廢
						@FormCategory = 1       --PR -->作廢請購 
				--if @return_value > 0
				--	Print N'請購單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '動支記錄作廢結果:失敗' 
				--else
				--	Print N'請購單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '動支記錄作廢結果:成功' 
		end


	--採購單/年度議價核發:傳送
	----[ERP].[usp_InsertBudgetPOUsed]
	if (SUBSTRING(@FormNum,1,2) = 'PO' or SUBSTRING(@FormNum,1,3) = 'BPR')  And @Action = 1
		begin
			print N'採購單/年度議價核發:傳送'
			if SUBSTRING(@FormNum,1,2) = 'PO'
				Begin
					Set @FormKind = 2
					--先將可能存在的動支記錄刪除
					Delete From CreditDepBudgetDetail Where FormNum = @FormNum And FormStatus = @Action
					--取得採購單的送貨層明細
					DECLARE PO_cursor CURSOR FOR   
					SELECT PO.PODeliveryID, PRDeliveryID,(Select Top 1 PRNum From PurchaseRequisition Where PRID in 
																				(Select PRID From PurchaseRequisitionDetail Where IsDelete = 0 And PRDetailID in 
																						(Select PRDetailID From PurchaseRequisitionDelivery Where IsDelete = 0 And ISNULL(CancelReason,'') = '' And PRDeliveryID = PO.PRDeliveryID))) 
							From PurchaseOrderDelivery PO Where PODetailID in
								(Select PODetailID From PurchaseOrderDetail Where POID in 
									(Select POID From PurchaseOrder where PONum = @FormNum ))
									And PO.PRDeliveryID in (Select PRDeliveryID From PurchaseRequisitionDelivery Where IsDelete = 0 And ISNULL(PO.CancelReason,'') = ''
													 And PRDetailID In (Select PRDetailID From PurchaseRequisitionDetail Where PRID in 
																																(Select PRID From PurchaseRequisition Where ProjectCategoryID In 
																																(Select ProjectCategoryID  From ProjectCategory Where Source = 'EPMS')))) 						
 
					OPEN PO_cursor  
  					FETCH NEXT FROM PO_cursor INTO @DeliveryNo,@SourceDeliveryNo,@SourceFormNum  
  
					WHILE @@FETCH_STATUS = 0  
					BEGIN  
						Print N'準備寫入預算動支' + '表單編號:' + @FormNum + '送貨層編號:' + cast(@DeliveryNo as varchar(20)) + '來源表單編號:' + @SourceFormNum + '來源送貨層編號:' + cast(@SourceDeliveryNo as varchar(20)) 
						Set @TransferDate = GETDate()
						EXEC	@return_value = [ERP].[usp_InsertBudgetPOUsed]
								@FormNum = @FormNum,
								@DeliveryNo = @DeliveryNo,
								@TransferDate = @TransferDate,
								@FormCategory = 2,   --PO
								@FormStatus = @Action,     --傳送 
								@FormKind = 2,       --採購單 
								@SourceFormNum = @SourceFormNum,
								@SourceDeliveryNo = @SourceDeliveryNo
						--if @return_value > 0
						--	Print N'採購單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '請購單編號:' + @SourceFormNum + ' 送貨層編號:' + @SourceDeliveryNo + '動支記錄結果:失敗' 
						--else
						--	Print N'採購單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '請購單編號:' + @SourceFormNum + ' 送貨層編號:' + @SourceDeliveryNo + '動支記錄結果:成功' 
	  					FETCH NEXT FROM PO_cursor INTO @DeliveryNo,@SourceDeliveryNo,@SourceFormNum  
					End
					CLOSE PO_cursor  
					DEALLOCATE PO_cursor  
				End
    else
				Begin
					Set @FormKind = 3
					--年度議價協議核發寫入動支記錄的表單編號是年度議價協議+核發次數
					Select @BpaNumReleaseNum = YC.BpaNum + Ltrim(CAST(YA.ReleaseNum as varchar(5))) from YearlyContract YC inner join YearlyApproved YA on YC.YCID = YA.YCID  where YA.BprNum =  @FormNum
					--先將可能存在的動支記錄刪除
					--先將可能存在的動支記錄刪除，用特別記下的BprFormNum(年度議價核發表單編號)來找
					Delete From CreditDepBudgetDetail Where BprFormNum = @FormNum And FormStatus = @Action
					--取得年度議價核發的送貨層明細
					DECLARE YA_cursor CURSOR FOR   
					SELECT YADeliveryID, PRDeliveryID,(Select Top 1 PRNum From PurchaseRequisition Where PRID in 
																				(Select PRID From PurchaseRequisitionDetail Where IsDelete = 0 And PRDetailID in 
																						(Select PRDetailID From PurchaseRequisitionDelivery Where IsDelete = 0 And isnull(CancelReason,'') = '' And PRDeliveryID = YA.PRDeliveryID))) 
							From YearlyApprovedDelivery YA Where YA.YADetailId in
								(Select YADetailId From YearlyApprovedDetail Where YAID in 
									(Select YAID From YearlyApproved where BprNum = @FormNum )) AND IsDelete = 0 And ISNULL(YA.CancelReason,'') = '' 
									And YA.PRDeliveryID in (Select PRDeliveryID From PurchaseRequisitionDelivery Where IsDelete = 0 
													 And PRDetailID In (Select PRDetailID From PurchaseRequisitionDetail Where PRID in 
																																(Select PRID From PurchaseRequisition Where ProjectCategoryID In 
																																(Select ProjectCategoryID  From ProjectCategory Where Source = 'EPMS')))) 						
  
					OPEN YA_cursor  
  					FETCH NEXT FROM YA_cursor INTO @DeliveryNo,@SourceDeliveryNo,@SourceFormNum  
  
					WHILE @@FETCH_STATUS = 0  
					BEGIN  
						Set @TransferDate = GETDate()
						EXEC	@return_value = [ERP].[usp_InsertBudgetPOUsed]
								@FormNum = @BpaNumReleaseNum,
								@DeliveryNo = @DeliveryNo,
								@TransferDate = @TransferDate,
								@FormCategory = 2,   --PO
								@FormStatus = @Action,     --傳送 
								@FormKind = 3,       --年度議價核發單 
								@SourceFormNum = @SourceFormNum,
								@SourceDeliveryNo = @SourceDeliveryNo,
								@BprFormNum = @FormNum
						--if @return_value > 0
						--	Print N'年度議價核發單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '請購單編號:' + @SourceFormNum + ' 送貨層編號:' + @SourceDeliveryNo + '動支記錄結果:失敗' 
						--else
						--	Print N'年度議價核發編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '請購單編號:' + @SourceFormNum + ' 送貨層編號:' + @SourceDeliveryNo + '動支記錄結果:成功' 
	  					FETCH NEXT FROM YA_cursor INTO @DeliveryNo,@SourceDeliveryNo,@SourceFormNum  
					End
					CLOSE YA_cursor  
					DEALLOCATE YA_cursor  
				End
		end


	--採購單/年度議價核發:銷案
	----[ERP].[usp_UpdateDismissalBudgetPOUsed]
		if (SUBSTRING(@FormNum,1,2) = 'PO' or SUBSTRING(@FormNum,1,3) = 'BPR')  And @Action = 3
		begin
			print N'採購單/年度議價核發:銷案'
			if SUBSTRING(@FormNum,1,2) = 'PO'
				Begin
					Set @FormKind = 2
					--先將可能存在的動支記錄刪除
					Delete From CreditDepBudgetDetail Where FormNum = @FormNum And FormStatus = @Action
					--取得採購單的送貨層明細
					DECLARE PO_cursor CURSOR FOR   
					SELECT PO.PODeliveryID From PurchaseOrderDelivery PO Where IsDelete = 0 AND isnull(CancelReason,'') = '' And PODetailID in
												(Select PODetailID From PurchaseOrderDetail Where IsDelete = 0 And POID in 
													(Select POID From PurchaseOrder where PONum = @FormNum ))

					--因為是銷案已經有正向的動支金額，所以不用看是否是行銷活動專案類別
					--And PO.PRDeliveryID in (Select PRDeliveryID From PurchaseRequisitionDelivery Where IsDelete = 0 And ISNULL(PO.CancelReason,'') = ''
					--And PRDetailID In (Select PRDetailID From PurchaseRequisitionDetail Where PRID in 
					--																		(Select PRID From PurchaseRequisition Where ProjectCategoryID In 
					--																		(Select ProjectCategoryID  From ProjectCategory Where Source = 'EPMS')))) 						
  
					OPEN PO_cursor  
  					FETCH NEXT FROM PO_cursor INTO @DeliveryNo
  
					WHILE @@FETCH_STATUS = 0  
					BEGIN  
						Set @TransferDate = GETDate()
						EXEC	@return_value = [ERP].[usp_UpdateDismissalBudgetPOUsed]
								@FormNum = @FormNum,
								@TransferDate = @TransferDate,
								@FormStatus = @Action,   --銷案
								@DeliveryNo = @DeliveryNo,
								@FormCategory = 2
						--if @return_value > 0
						--	Print N'採購單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '動支記錄銷案結果:失敗' 
						--else
						--	Print N'採購單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '動支記錄銷案結果:成功' 
	  					FETCH NEXT FROM PO_cursor INTO @DeliveryNo  
					End
					CLOSE PO_cursor  
					DEALLOCATE PO_cursor  
				End
            else  --年度議價核發銷案
				Begin
					Set @FormKind = 3
					--年度議價協議核發寫入動支記錄的表單編號是年度議價協議+核發次數
					--Select @BpaNumReleaseNum = YC.BpaNum + Ltrim(CAST(YA.ReleaseNum as varchar(5))) from YearlyContract YC inner join YearlyApproved YA on yc.YCID = YA.YCID  where YA.BprNum =  @FormNum
					--先將可能存在的動支記錄刪除，用特別記下的BprFormNum(年度議價核發表單編號)來找
					Delete From CreditDepBudgetDetail Where BprFormNum = @FormNum And FormStatus = @Action
					--取得年度議價核發的送貨層明細
					DECLARE YA_cursor CURSOR FOR   
					SELECT YADeliveryID	From YearlyApprovedDelivery YA Where IsDelete = 0 AND isnull(CancelReason,'') = '' And YA.YADetailId in
								(Select YADetailId From YearlyApprovedDetail Where IsDelete = 0 And YAID in 
									(Select YAID From YearlyApproved where BprNum = @FormNum ))

						--因為是銷案已經有正向的動支金額，所以不用看是否是行銷活動專案類別
						--And YA.PRDeliveryID in (Select PRDeliveryID From PurchaseRequisitionDelivery Where IsDelete = 0 And ISNULL(YA.CancelReason,'') = ''
						--				 And PRDetailID In (Select PRDetailID From PurchaseRequisitionDetail Where PRID in 
						--																							(Select PRID From PurchaseRequisition Where ProjectCategoryID In 
						--																							(Select ProjectCategoryID  From ProjectCategory Where Source = 'EPMS')))) 						

					OPEN YA_cursor  
  					FETCH NEXT FROM YA_cursor INTO @DeliveryNo 
  
					WHILE @@FETCH_STATUS = 0  
					BEGIN  
						Set @TransferDate = GETDate()
						EXEC	@return_value = [ERP].[usp_UpdateDismissalBudgetBPRUsed]
								@FormNum = @FormNum,
								@TransferDate = @TransferDate,
								@FormStatus = @Action,   --銷案
								@DeliveryNo = @DeliveryNo,
								@FormCategory = 2
						--if @return_value > 0
						--	Print N'年度議價核發單編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '動支記錄銷案結果:失敗' 
						--else
						--	Print N'年度議價核發編號:' + @FormNum + ' 送貨層編號:' + @DeliveryNo + '動支記錄銷案結果:成功' 
	  					FETCH NEXT FROM YA_cursor INTO @DeliveryNo
					End
					CLOSE YA_cursor  
					DEALLOCATE YA_cursor  
				End
		end

	--採購單/年度議價核發:作廢
	----[ERP].[usp_UpdateVoidedBudgetPOUsed]
		if (SUBSTRING(@FormNum,1,2) = 'PO' or SUBSTRING(@FormNum,1,3) = 'BPR')  And @Action = 2
		begin
			print N'採購單/年度議價核發:作廢'
			if SUBSTRING(@FormNum,1,2) = 'PO'
				Begin
					Set @FormKind = 2
					--先將可能存在的動支記錄刪除
					Delete From CreditDepBudgetDetail Where SourceFormNum = @FormNum And DeliveryNo = @VoidedDeliveryNo And FormCategory = 2 And FormStatus = @Action
						Set @TransferDate = GETDate()
						EXEC	@return_value = [ERP].[usp_UpdateVoidedBudgetPOUsed]
								@SourceFormNum = @FormNum,
								@SourceDeliveryNo = @VoidedDeliveryNo,
								@TransferDate = @TransferDate,
								@FormStatus = @Action,          --作廢
								@FormCategory = 2
				End
            else  --年度議價核發作廢
				Begin
					Set @FormKind = 3
					--年度議價協議核發寫入動支記錄的表單編號是年度議價協議+核發次數
					Select @BpaNumReleaseNum = YC.BpaNum + Ltrim(CAST(YA.ReleaseNum as varchar(5))) from YearlyContract YC inner join YearlyApproved YA on yc.YCID = YA.YCID  where YA.BprNum =  @FormNum
					--先將可能存在的動支記錄刪除
					Delete From CreditDepBudgetDetail Where BprFormNum = @FormNum And DeliveryNo = @VoidedDeliveryNo And FormCategory = 2 And FormStatus = @Action
						Set @TransferDate = GETDate()
						EXEC	@return_value = [ERP].[usp_UpdateVoidedBudgetBPRUsed]
								@SourceFormNum = @FormNum,
								@SourceDeliveryNo = @VoidedDeliveryNo,
								@TransferDate = @TransferDate,
								@FormStatus = @Action,          --作廢
								@FormCategory = 2,
								@BpaFormNum = @BpaNumReleaseNum
				End
		end

--***  請採購預算動支 ***--  

	Declare @BudgetAmount Decimal(15,4),@RemainBudget Decimal(15,4), @ChargeDept varchar(10)  --定義是否超過預算相關欄位
	Declare @Quantity Decimal(19,6)  --請款數量
	Select @ExpenseKind = ExpenseKind From EXPMaster Where EXPID = @FormID

	--廠商請款-請購:傳送、銷案
	----[ERP].[usp_InsertBudgetAPEPOUsed]
		if SUBSTRING(@FormNum,1,3) = 'EPO' And @ExpenseKind = 'PO_STD' 
		begin
			print N'廠商請款-請購:傳送、銷案'
				--先將可能存在的動支記錄刪除
				Delete From CreditDepBudgetDetail Where FormNum = @FormNum And FormStatus = @Action And FormCategory = 3
				--判斷是否超過可用預算金額
				Set @RemainBudget = 0
				if @Action = 1
					Begin
						--取得此次動支金額	
						Select @BudgetAmount = Sum(OriginalAmortizationTWDAmount) from AmortizationDetail where LineNum 
						In (Select LineNum From EPODetail EPO Where IsDelete = 0 And EPO.EPOID in (Select EPOID From EPOMaster Where EPONum = @FormNum)) And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 
						--取得成本利潤中心
						Select Top 1 @ChargeDept = CostProfitCenter from AmortizationDetail where LineNum 
						In (Select LineNum From EPODetail EPO Where IsDelete = 0 And EPO.EPOID in (Select EPOID From EPOMaster Where EPONum = @FormNum)) And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 

						EXEC	@RemainBudget = [ERP].[usp_GetCreditBudgetVerify]
								@TransferDate = @TransferDate,
								@BudgetAmount = @BudgetAmount,
								@CostProfitCenter = @ChargeDept
					End

                if @RemainBudget > 0  --@RemainBudget是0則未超過可用預算，> 0則@RemainBudget則為其可用預算餘額
					Begin
						Update @Result Set ReturnStatus = 3,RemainBudget = @RemainBudget 
					End
				Else
					Begin
						--取得請款單的明細
						DECLARE EPO_cursor CURSOR FOR   
						SELECT EPO.LineNum, EPO.PODeliveryID,EPO.PONum,EPO.DiscountQuantity
								From EPODetail EPO Where IsDelete = 0 AND LineNum in (Select LineNum From AmortizationDetail Where SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K'))
									And EPO.EPOID in (Select EPOID From EPOMaster Where EPONum = @FormNum) 
  
						OPEN EPO_cursor  
  						FETCH NEXT FROM EPO_cursor INTO @DeliveryNo,@SourceDeliveryNo,@SourceFormNum,@Quantity  
  
						WHILE @@FETCH_STATUS = 0  
						BEGIN  
							Set @TransferDate = GETDate()
							EXEC	@return_value = [ERP].[usp_InsertBudgetAPEPOUsed]
									@FormNum = @FormNum,
									@DeliveryNo = @DeliveryNo,
									@TransferDate = @TransferDate,
									@FormCategory = 3,       --AP類
									@FormStatus = @Action,
									@FormKind = 5,    --廠商請款請購
									@SourceFormNum = @SourceFormNum,
									@SourceDeliveryNo = @SourceDeliveryNo,
									@Quantity = @Quantity
							--if @return_value > 0
							--   Print N'請款購單編號:' + @FormNum + ' 請款單明細編號:' + @DeliveryNo + '採購單編號:' + @SourceFormNum + ' 採購單送貨層編號:' + @SourceDeliveryNo + '動支記錄結果:失敗' 
							--else
							--Print N'請款購單編號:' + @FormNum + ' 請款單明細編號:' + @DeliveryNo + '採購單編號:' + @SourceFormNum + ' 採購單送貨層編號:' + @SourceDeliveryNo + '動支記錄結果:成功' 
	  						FETCH NEXT FROM EPO_cursor INTO @DeliveryNo,@SourceDeliveryNo,@SourceFormNum,@Quantity  
						End
						CLOSE EPO_cursor  
						DEALLOCATE EPO_cursor  
					End
		end

	--廠商請款-請購(折讓):傳送、銷案
	----[ERP].[usp_InsertBudgetAPEPODiscountUsed]
		if SUBSTRING(@FormNum,1,3) = 'EPO' And @ExpenseKind = 'PO_CM_RETURN'
		begin
			print N'廠商請款-請購(折讓):傳送、銷案'
				--先將可能存在的動支記錄刪除
				Delete From CreditDepBudgetDetail Where FormNum = @FormNum And FormStatus = @Action AND FormCategory = 3
				--判斷是否超過可用預算金額
				Set @RemainBudget = 0
				if @Action = 3
					Begin
						--取得此次動支金額	
						Select @BudgetAmount = Sum(OriginalAmortizationTWDAmount) from AmortizationDetail where LineNum 
						In (Select LineNum From EPODetail EPO Where IsDelete = 0 And EPO.EPOID in (Select EPOID From EPOMaster Where EPONum = @FormNum)) And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 
						--取得成本利潤中心
						Select Top 1 @ChargeDept = CostProfitCenter from AmortizationDetail where LineNum 
						In (Select LineNum From EPODetail EPO Where IsDelete = 0 And EPO.EPOID in (Select EPOID From EPOMaster Where EPONum = @FormNum)) And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 

						EXEC	@RemainBudget = [ERP].[usp_GetCreditBudgetVerify]
								@TransferDate = @TransferDate,
								@BudgetAmount = @BudgetAmount,
								@CostProfitCenter = @ChargeDept
					End
                if @RemainBudget > 0  --超過可用預算
					Begin
						Update @Result Set ReturnStatus = 3,RemainBudget = @RemainBudget 
					End
				Else
					Begin
						DECLARE EPO_cursor CURSOR FOR   
						SELECT A.ADetailID From AmortizationDetail A Where LineNum in (Select LineNum From AmortizationDetail Where SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K'))
																	And IsDelete = 0 And LineNum In (Select LineNum From EPODetail Where IsDelete = 0 And EPOID in (Select EPOID From EPOMaster Where EPONum = @FormNum)) 
  						OPEN EPO_cursor  
  						FETCH NEXT FROM EPO_cursor INTO @DeliveryNo
  
						WHILE @@FETCH_STATUS = 0  
						BEGIN  
							Set @TransferDate = GETDate()
							EXEC	@return_value = [ERP].[usp_InsertBudgetAPEPODiscountUsed]
									@FormNum = @FormNum,
									@DeliveryNo = @DeliveryNo,
									@TransferDate = @TransferDate,
									@FormCategory = 3,      --AP  
									@FormStatus = @Action

							--if @return_value > 0
							--	Print N'請款購單編號:' + @FormNum + ' 請款單分攤明細編號:'+ @DeliveryNo + '動支記錄結果:失敗' 
							--else
							--	Print N'請款購單編號:' + @FormNum + ' 請款單分攤明細編號:' + @DeliveryNo + '動支記錄結果:成功' 
	  						FETCH NEXT FROM EPO_cursor INTO @DeliveryNo
						End
						CLOSE EPO_cursor  
						DEALLOCATE EPO_cursor  
					End
		end

	--廠商請款-非請購:傳送、銷案
	----[ERP].[usp_InsertBudgetAPENPUsed]
		if SUBSTRING(@FormNum,1,3) = 'ENP' And @ExpenseKind = 'PO_CM_EXP'
		begin
			print N'廠商請款-非請購:傳送、銷案'
				--先將可能存在的動支記錄刪除
				Delete From CreditDepBudgetDetail Where FormNum = @FormNum And FormStatus = @Action AND FormCategory = 3
				--判斷是否超過可用預算金額
				Set @RemainBudget = 0
				if @Action = 1
					Begin
						--取得此次動支金額	
						Select @BudgetAmount = Sum(DistamountTWD) from FiisAmortization where LineNum 
						In (Select LineNum From ENPDetail ENP Where IsDelete = 0 And ENP.EID in (Select EID From ENPMaster Where ENPNum = @FormNum)) And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 
						--取得成本利潤中心
						Select Top 1 @ChargeDept = CostCenter from FiisAmortization where LineNum 
						In (Select LineNum From ENPDetail ENP Where IsDelete = 0 And ENP.EID in (Select EID From ENPMaster Where ENPNum = @FormNum)) And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 

						EXEC	@RemainBudget = [ERP].[usp_GetCreditBudgetVerify]
								@TransferDate = @TransferDate,
								@BudgetAmount = @BudgetAmount,
								@CostProfitCenter = @ChargeDept
					End
                if @RemainBudget > 0  --超過可用預算
					Begin
						Update @Result Set ReturnStatus = 3,RemainBudget = @RemainBudget 
					End
				Else
					Begin
						--取得分攤明細的請款明細編號、分攤編號
						DECLARE ENP_cursor CURSOR FOR   
						SELECT F.LineNum, F.DistNum From FiisAmortization F Where LineNum In (
							   Select LineNum From ENPDetail Where IsDelete = 0 And EID in (Select EID From ENPMaster Where ENPNum = @FormNum))
  
						OPEN ENP_cursor  
  						FETCH NEXT FROM ENP_cursor INTO @SourceDeliveryNo,@DeliveryNo
  
						WHILE @@FETCH_STATUS = 0  
						BEGIN  
							Set @TransferDate = GETDate()
							EXEC	@return_value = [ERP].[usp_InsertBudgetAPENPUsed]
							@FormNum = @FormNum,
							@DeliveryNo = @DeliveryNo,
							@TransferDate = @TransferDate,
							@FormCategory = 3,   --AP
							@FormStatus = @Action,
							@SourceDeliveryNo = @SourceDeliveryNo
							--if @return_value > 0
							--	Print N'請款購單編號:' + @FormNum + ' 請款單明細編號:' + @SourceDeliveryNo + ' 分攤明細編號:' + @SourceDeliveryNo + '動支記錄結果:失敗' 
							--else
							--	Print N'請款購單編號:' + @FormNum + ' 請款單明細編號:' + @SourceDeliveryNo + ' 分攤明細編號:' + @SourceDeliveryNo + '動支記錄結果:成功' 
	  						FETCH NEXT FROM ENP_cursor INTO @SourceDeliveryNo,@DeliveryNo
						End
						CLOSE ENP_cursor  
						DEALLOCATE ENP_cursor  
					End

		end

	--廠商請款-非請購(折讓):傳送、銷案
	----[ERP].[usp_InsertBudgetAPENPDiscountUsed]
		if SUBSTRING(@FormNum,1,3) = 'ENP' And @ExpenseKind = 'NPO_CM_EXP'
		begin
			print N'廠商請款-非請購(折讓):傳送、銷案'
				--先將可能存在的動支記錄刪除
				Delete From CreditDepBudgetDetail Where FormNum = @FormNum And FormStatus = @Action AND FormCategory = 3
								--判斷是否超過可用預算金額
				Set @RemainBudget = 0
				if @Action = 3
					Begin
						--取得此次動支金額	
						Select @BudgetAmount = Sum(DistamountTWD) from FiisAmortization where LineNum 
						In (Select LineNum From ENPDetail ENP Where IsDelete = 0 And ENP.EID in (Select EID From ENPMaster Where ENPNum = @FormNum)) And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 
						--取得成本利潤中心
						Select Top 1 @ChargeDept = CostCenter from FiisAmortization where LineNum 
						In (Select LineNum From ENPDetail ENP Where IsDelete = 0 And ENP.EID in (Select EID From ENPMaster Where ENPNum = @FormNum)) And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 

						EXEC	@RemainBudget = [ERP].[usp_GetCreditBudgetVerify]
								@TransferDate = @TransferDate,
								@BudgetAmount = @BudgetAmount,
								@CostProfitCenter = @ChargeDept
					End
                if @RemainBudget > 0  --超過可用預算
					Begin
						Update @Result Set ReturnStatus = 3,RemainBudget = @RemainBudget 
					End
				Else
					Begin
						--取得分攤明細的請款明細編號、分攤編號
						DECLARE ENP_cursor CURSOR FOR   
						SELECT F.LineNum, F.DistNum From FiisAmortization F Where LineNum In (
							   Select LineNum From ENPDetail Where IsDelete = 0 And EID in (Select EID From ENPMaster Where ENPNum = @FormNum))
  
						OPEN ENP_cursor  
  						FETCH NEXT FROM ENP_cursor INTO @SourceDeliveryNo,@DeliveryNo
  
						WHILE @@FETCH_STATUS = 0  
						BEGIN  
							Set @TransferDate = GETDate()
							EXEC	@return_value = [ERP].[usp_InsertBudgetAPENPDiscountUsed]
									@FormNum = @FormNum,
									@DeliveryNo = @DeliveryNo,
									@TransferDate = @TransferDate,
									@FormCategory = 3,      --AP
									@FormStatus = @Action,
									@SourceDeliveryNo = @SourceDeliveryNo
							--if @return_value > 0
							--	Print N'請款購單編號:' + @FormNum + ' 請款單明細編號:' + @SourceDeliveryNo + ' 分攤明細編號:' + @SourceDeliveryNo + '動支記錄結果:失敗' 
							--else
							--	Print N'請款購單編號:' + @FormNum + ' 請款單明細編號:' + @SourceDeliveryNo + ' 分攤明細編號:' + @SourceDeliveryNo + '動支記錄結果:成功' 
	  						FETCH NEXT FROM ENP_cursor INTO @SourceDeliveryNo,@DeliveryNo
						End
						CLOSE ENP_cursor  
						DEALLOCATE ENP_cursor  
				End
		end
	Select * From @Result
END