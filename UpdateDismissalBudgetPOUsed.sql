USE [OA]
GO
/****** Object:  StoredProcedure [ERP].[usp_UpdateDismissalBudgetPOUsed]    Script Date: 2019/4/23 下午 01:33:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Louis
-- Create date: 2018/11/01
-- Description:	銷案信用卡部預算動支明細-採購單
-- =============================================
ALTER PROCEDURE [ERP].[usp_UpdateDismissalBudgetPOUsed] 
		 @FormNum varchar(20)
		,@TransferDate date
		,@FormStatus int
		,@DeliveryNo int
		,@FormCategory int
AS
BEGIN
	Declare	 @count int
	Declare @Result table (ReturnStatus varchar(50),ReturnMessage varchar(100),FormNum varchar(20),DeliveryNo int)
	Print 'N銷案單編號:' 

		Declare @prErr int,@poErr int
		Begin Transaction
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		insert into CreditDepBudgetDetail(
			CostProfitCenter 
			,CostProfitCenterName
			,FormCategory
			,ProjectCategoryID
			,FormNum
			,DeliveryNo
			,FormStatus
			,TransferDate
			,BudgetAmount   
			,Included
			,FormKind
			,SourceFormNum
			,SourceDeliveryNo
			,FormRole
			,CreateTime 
		)
		Select top 1 CostProfitCenter 
			,CostProfitCenterName
			,FormCategory 
			,ProjectCategoryID
			,FormNum
			,DeliveryNo
			,FormStatus = @FormStatus
			,TransferDate = @TransferDate    
			,BudgetAmount = -(BudgetAmount)
			,Included
			,FormKind
			,SourceFormNum
			,SourceDeliveryNo
			,2
			,CreateTime = Getdate()
		From CreditDepBudgetDetail where FormNum = @FormNum and FormStatus = 1 and FormCategory = 1 and DeliveryNo = @DeliveryNo    --針對PR銷案
		set @prErr = @@ERROR

		insert into CreditDepBudgetDetail(
			CostProfitCenter 
			,CostProfitCenterName
			,FormCategory
			,ProjectCategoryID
			,FormNum
			,DeliveryNo
			,FormStatus
			,TransferDate
			,BudgetAmount   
			,Included
			,FormKind
			,SourceFormNum
			,SourceDeliveryNo
			,FormRole
			,CreateTime 
		)
		Select top 1 CostProfitCenter 
			,CostProfitCenterName
			,FormCategory
			,ProjectCategoryID
			,FormNum
			,DeliveryNo
			,FormStatus = @FormStatus
			,TransferDate = @TransferDate    
			,BudgetAmount = -(BudgetAmount)
			,Included
			,FormKind
			,SourceFormNum
			,SourceDeliveryNo
			,2
			,CreateTime = Getdate()
		From CreditDepBudgetDetail where FormNum = @FormNum and FormStatus = 1 and FormCategory = 2 and DeliveryNo = @DeliveryNo    --針對PO銷案

		set @poErr = @@ERROR
		if @prErr = 0 and @poErr = 0
			Begin
				set @count = @@TRANCOUNT
				Commit Transaction
			End
		else
			Begin
				set @count = 0
				insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E6','寫入資料庫失敗',@FormNum,@DeliveryNo)
				RollBack Transaction
			End

--取得最後結果，若是無錯誤則回傳空值		
--Select * from @Result
if @count > 0
	return 1
else
	return 0

END
