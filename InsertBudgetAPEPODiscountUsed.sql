USE [OA]
GO
/****** Object:  StoredProcedure [ERP].[usp_InsertBudgetAPEPODiscountUsed]    Script Date: 2019/4/23 下午 01:36:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Louis
-- Create date: 2018/12/03
-- Description:	新增信用卡部預算動支明細檔-廠商請款-請購折讓:傳送及銷案
-- =============================================
ALTER PROCEDURE [ERP].[usp_InsertBudgetAPEPODiscountUsed]  
		 @FormNum varchar(20)            --表單編號
		,@DeliveryNo int                 --分攤明細編號  
		,@TransferDate date              --傳送日期
		,@FormCategory int               --1.PR,2.PO,3.AP   
		,@FormStatus int                 --1.傳送,2.作廢,3.銷案
AS
Begin
Declare	 @count int
		,@ChargeDept  varchar(10) 
		,@ChargeDeptName nvarchar(50) 
		,@ProjectCategoryID uniqueidentifier 
		,@BudgetAmount decimal(15,4) 
		,@OverBudget int
		,@BudgetYearMonth varchar(6)
		,@ExpenseAttribute varchar(2)
		,@ExpenseAttributeName nvarchar(100)

Set @BudgetYearMonth = cast(Year(@TransferDate) as varchar) + RIGHT(REPLICATE('0', 2) + cast(Month(@TransferDate) as varchar),2)
--定義回傳結果變數Table
Declare @Result table (ReturnStatus varchar(50),ReturnMessage nvarchar(100),FormNum varchar(20),DeliveryNo int)

--檢查行銷活動專案
  Select @ProjectCategoryID = ProjectCategoryID From ProjectCategory Where Code in (Select Top 1 ProjectCategory  From EPODetail  Where LineNum in ( Select LineNum From AmortizationDetail Where IsDelete = 0 And ADetailID = @DeliveryNo)) 

--檢查成本與利潤中心
  Select @ExpenseAttribute = ExpenseAttribute,@ExpenseAttributeName = ExpenseAttributeName,  @ChargeDept = CostProfitCenter,@ChargeDeptName = CostProfitCenterName from AmortizationDetail where IsDelete = 0 And ADetailID = @DeliveryNo And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 

set @count = 0 
--取得分攤明細金額
Select @count = COUNT(*) from AmortizationDetail where IsDelete = 0 And ADetailID = @DeliveryNo And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 
if @count = 0
	begin
		insert into @Result(ReturnStatus,ReturnMessage,FormNum,DeliveryNo) values('E7',N'無分攤明細金額',@FormNum,@DeliveryNo)
	end
else
	begin
		Select @BudgetAmount = OriginalAmortizationTWDAmount from AmortizationDetail where IsDelete = 0 And ADetailID = @DeliveryNo And SUBSTRING(ExpenseAttribute,1,1) in ('A','B','C','D','E','F','G','H','I','J','K') 
		if @FormStatus = 3  --銷案時將金額轉向回來
		   begin
				Set @BudgetAmount = -@BudgetAmount
		   end		
	end

	--更新月累記動支金額,因為呼叫本程序前做了刪除
	Update CreditDepBudget Set MonthAddBudget = (Select Sum(BudgetAmount) From CreditDepBudgetDetail Where CostProfitCenter = @ChargeDept And cast(Year(@TransferDate) as varchar) + RIGHT(REPLICATE('0', 2) + cast(Month(@TransferDate) as varchar),2) = @BudgetYearMonth And Included = 1)
			Where CostProfitCenter = @ChargeDept And BudgetYearMonth = @BudgetYearMonth
  Set @count = 0
  Select @count = Count(*) from @Result
  if @count = 0 --都沒有錯誤執行預算動支寫入
     begin
				--廠商請款-請購一般折讓
				INSERT INTO ERP.CreditDepBudgetDetail(
						CostProfitCenter 
					,CostProfitCenterName 
					,ExpenseAttribute
					,ExpenseAttributeName
					,ProjectCategoryID	
					,TransferDate	
					,FormCategory	
					,FormNum	
					,DeliveryNo	
					,FormStatus	
					,BudgetAmount	
					,Included
					,FormKind
					,FormRole
					,CreateTime	
					)
				VALUES (
						@ChargeDept 
						,@ChargeDeptName 
						,@ExpenseAttribute
						,@ExpenseAttributeName
						,@ProjectCategoryID
						,@TransferDate	
						,3                     --AP	
						,@FormNum	
						,@DeliveryNo	
						,@FormStatus	
						,@BudgetAmount
						,1
						,6          --廠商請款 -請購(折讓)
						,4          --請款人員  
					,GETDATE()	
					)
					--判斷為本月首次動支記錄呼叫更新信用卡部門預算動支主檔
					Set @count = 0
					SELECT @count = Count(*) From CreditDepBudgetDetail Where CostProfitCenter = @ChargeDept And CostProfitCenter In (Select CostProfitCenter From CreditDepBudget Where BudgetYearMonth = @BudgetYearMonth)
					if @count = 0
						Begin
							--更新信用卡部門預算動支主檔
						EXEC	@count = [ERP].[usp_UpdateCreditDepBudget]
								@TransferDate = @TransferDate,
								@CostProfitCenter = @ChargeDept
						End
      end

  --若無錯誤更新預算主檔
  set @count = 0
  Select @count = Count(*) From @Result
  If @count = 0
	 Begin
		Update CreditDepBudget Set MonthAddBudget = (Select Sum(BudgetAmount) From CreditDepBudgetDetail Where CostProfitCenter = @ChargeDept And cast(Year(@TransferDate) as varchar) + RIGHT(REPLICATE('0', 2) + cast(Month(@TransferDate) as varchar),2) = @BudgetYearMonth And Included = 1)
			Where CostProfitCenter = @ChargeDept And BudgetYearMonth = @BudgetYearMonth
	 End

  --取得最後結果，若是無錯誤則回傳空值		
  --Select * from @Result
  	if @count > 0
		return 1
	else
		return 0

END
